X-UniTMX: A TMX importer for Unity3D
=======================================

X-UniTMX is a TMX file importer to load files from [tiled map editor] into Unity, originally developed for [Twisted Potions: Double Trouble] by [PolCPP]. 

This fork aims to convert [XTiled] (XNA) to Unity3D and merge it with UniTMX. It's currently under development. Please be aware that this project started using an old version of XTiled, when it was called TiledLib.

Thanks to [fontmas] for helping me update this project, for the prefab loading code and for the little game example included!

You can check for more information and some Guides on the [project's wiki](https://bitbucket.org/Chaoseiro/x-unitmx/wiki/Home)
--------

Important
--------
There are 2 branches, *master* and *Unity2D*. As you can imagine, the *Unity2D* branch contains code that runs on Unity3D 4.3+ only, using Unity's 2D features, while *master* is 4.0+.

Features
--------

   - Fully reads, imports and generates a map created with Tiled Map Editor (Orthogonal, Isometric or Staggered), you don't need to create a mesh neither any material. Please note that the tileset will be set to use the TileWidth as the "Pixels Per Unit" setting, so adjust any other sprite accordingly.
   - Can automatically generate Colliders (or triggers).
   - Exposed tile map informations, layers and objects.
   - Supports XML, CSV, Base64 Uncompressed and Compressed maps.
   - Supports multiple Tile Sets per Tile Layer.
   - Supports Object rotation and Tile objects.

Limitations
-----------

  - Doesn't support animated tiles from tiled yet, but and example using a custom script is included.
  - Doesn't support tile collision yet.
  - **Tile Sets MUST be inside Resources folder! See sample project structure.**


Installation
------------

   - Either copy X-UniTMX folder into your assets folder or import X-UniTMX.unitypackage as a Unity Package.


Usage
-----

### In Tiled Map editor:

#### Tiles

   - Load your tilemap
   - Paint some tiles in different layers

#### Collisions:
   - Use an Object Layer named as Collisions to automatically generate Unity Colliders (by checking 'Generate Colliders' checkbox).
		- Every object inside this layer will generate a collider!
   - You can use multiple Colliders layers, just add them to the Collider Layers list in the Inspector.

#### Finally

   - Save.
   - Rename the tmx file as xml. Tiled can open maps with xml extension, so you can use it freely and delete the tmx file.

### Unity (Visual)

   - Setup your 2d Scene for 2d sprites (orthographic camera etc), if you want take a look on how is the scene built on [Twisted potions: Double trouble].
   - **Important:** Use *Sprite* as your tilesets texture type!
   - Create an empty GameObject.
   - Now click on *Component*->*Scripts*->*Tiled Map Component*
   - On the GameObject inspector you'll [find a new set of options that look like this] 
   - Select the tilemap XML that you created earlier on tiled and hit Import tiles. Check
     Generate colliders according to your needs.

### Unity (Code)
   
   - Setup your 2d Scene for 2d sprites (orthographic camera etc), if you want take a look on how is the scene built on [Twisted potions: Double trouble].
   - **Important:** Use *Sprite* as your tilesets texture type!
   - Create a map loader script or modify the *MapLoader* script included inside *Utilities* folder.
   - Run your project!

*There are also some example assets that you can check if you have any doubts*

*In case you want to run the importer from inside the game it's also possible, check how the Editor/TileMapEditor.cs works, but be aware that the script is not optimized so you may have troubles with the GC*

*You'll will also have access to all tile map informations, including tiles, tilesets, objects etc, just take a look at the files inside Code/*

  [fontmas]: https://bitbucket.org/fontmas
  [tiled map editor]: http://mapeditor.org/
  [twisted potions: double trouble]: https://bitbucket.org/PolCPP/twisted-potions-double-trouble/
  [PolCPP]: https://bitbucket.org/PolCPP
  [XTiled]: https://bitbucket.org/vinull/xtiled
  [find a new set of options that look like this]: https://bitbucket.org/Chaoseiro/x-unitmx/src/e2142eae84ee4cbcc9dd1570810427c83b2be321/TiledMapComponentScreenshot.PNG?at=Unity2D
  [as in this image]: https://bitbucket.org/Chaoseiro/x-unitmx/src/fee4d5e7b270b3a59d1f9f9eeb92edf84cd09b64/TileSetTextureConfigs.PNG?at=Unity2D